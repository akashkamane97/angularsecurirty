import { TestBed, async } from '@angular/core/testing';
import { RecipeItemComponent } from './recipe-item.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecipeItemComponent],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(RecipeItemComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'ng-complete-guide-update'`, () => {
    const fixture = TestBed.createComponent(RecipeItemComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('ng-complete-guide-update');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(RecipeItemComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain(
      'Welcome to ng-complete-guide-update!'
    );
  });
});
