import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RecipeItemComponent } from './recipe-item.component';

@NgModule({
  declarations: [RecipeItemComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [RecipeItemComponent],
})
export class AppModule {}
