import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Post } from './post.model';
import { PostsService } from './post.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css'],
})
export class RecipeItemComponent implements OnInit {
  loadedPosts: Post[] = [];

  constructor(private http: HttpClient, private postsService: PostsService) {}

  ngOnInit() {
    this.fetchPosts();
  }

  onCreatePost(postData: Post) {
    // Send Http request
    this.postsService.createAndStorePost(
      postData.id,
      postData.firstname,
      postData.city,
      postData.dateOfBirth,
      postData.email
    );
  }

  onFetchPosts() {
    this.fetchPosts();
  }

  private fetchPosts() {
    this.http
      .get<{ [key: string]: Post }>('http://localhost:8002/employee/fetchAll')
      .pipe(
        map((responseData: { [key: string]: Post }) => {
          const postArray: Post[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              postArray.push({ ...responseData[key], id: key });
            }
          }
          return postArray;
        })
      )
      .subscribe((posts) => {
        this.loadedPosts = posts;
      });
  }

  onClearPosts() {
    // Send Http request
  }
}

// .pipe(
//   map((responseData: { [key: string]: Post }) => {
//     const postArray: Post[] = [];
//     for (const key in responseData) {
//       if (responseData.hasOwnProperty(key)) {
//         postArray.push({ ...responseData[key], data: key });
//       }
//     }
//   })
// )
