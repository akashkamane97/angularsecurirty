export interface Post {
  id?: string;
  firstname: string;
  email: string;
  city: string;
  dateOfBirth: string;
}
