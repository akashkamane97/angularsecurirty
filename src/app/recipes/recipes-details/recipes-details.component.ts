import { Component, NgModule, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { from, Observable } from 'rxjs';
// import { ModelDismissResponse, NgbModel } from '@ng-bootstrap/ngbootstrap';
import { map } from 'rxjs/operators';
import { User } from './user.model';

@Component({
  selector: 'app-recipes-details',
  templateUrl: './recipes-details.component.html',
  styleUrls: ['./recipes-details.component.css'],
})
export class RecipesDetailsComponent {
  constructor(private http: HttpClient) {}

  onAddEmployee(postData: {
    id: number;
    firstname: string;
    email: string;
    city: string;
  }) {
    this.http
      .post<{ [key: string]: User }>(
        'http://localhost:8002/employee/addEmployee',
        postData
      )
      .pipe(
        map((responseData: { [key: string]: User }) => {
          const postArray: User[] = [];
          for (const key in responseData) {
            if (responseData.hasOwnProperty(key)) {
              postArray.push({ ...responseData[key], id: key });
            }
          }
        })
      )
      .subscribe((responseData) => {
        console.log(responseData);
      });
  }
}
