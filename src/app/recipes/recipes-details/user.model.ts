export interface User {
  id?: string;
  firstname: string;
  email: string;
  city: string;
  dateOfBirth: string;
}
