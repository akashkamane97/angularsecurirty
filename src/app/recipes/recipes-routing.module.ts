import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecipesComponent } from './recipes.component';
import { AuthComponent } from '../auth/auth.component';

const appRoutes: Routes = [
  { path: '', component: AuthComponent, pathMatch: 'full' },
  { path: 'recipes', component: RecipesComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
